#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	
	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	cout << "Generated array: " << endl;
	cout << "Unordered: ";
	for (int i = 0; i < unordered.getSize(); i++)
		cout << unordered[i] << "   ";
	cout << "\nOrdered: ";
	for (int i = 0; i < ordered.getSize(); i++)
		cout << ordered[i] << "   ";
	cout << " " << endl;
	cout << "What do you want to do?" << endl;
	cout << "1. Remove element at index" << endl;
	cout << "2. Search for an element" << endl;
	cout <<	"3. Expand and generate random values" << endl;
	int choice;
	cin >> choice;
	switch (choice)
	{
	case 1:
		cout << "Please enter the index to be removed" << endl;
		int userinput;
		cin >> userinput;
		cout << endl;

		unordered.remove(userinput);
		ordered.remove(userinput);
		cout << "Unordered: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << "   ";
		cout << "\nOrdered: ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << "   ";
		
		break;
	case 2:
		cout << "\n\nEnter number to search: ";
		int input;
		cin >> input;
		cout << endl;

		cout << "Unordered Array(Linear Search):\n";
		int result = unordered.linearSearch(input);
		if (result >= 0)
			cout << input << " was found at index " << result << ".\n";
		else
			cout << input << " not found." << endl;

		cout << "Ordered Array(Binary Search):\n";
		result = ordered.binarySearch(input);
		if (result >= 0)
			cout << input << " was found at index " << result << ".\n";
		else
			cout << input << " not found." << endl;
		break;
	

	}
	
	system("pause");
}